//
//  TestingPipeLineApp.swift
//  TestingPipeLine
//
//  Created by Khanna, Videh Rakesh Rakesh on 01/12/21.
//

import SwiftUI

@main
struct TestingPipeLineApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
